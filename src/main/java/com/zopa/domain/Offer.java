package com.zopa.domain;

import java.math.BigDecimal;

public class Offer {
	
	private String lender;
	private BigDecimal rate;
	private BigDecimal available;
	
	public Offer(String lender, BigDecimal rate, BigDecimal available) {
		super();
		this.lender = lender;
		this.rate = rate;
		this.available = available;
	}

	public String getLender() {
		return lender;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public BigDecimal getAvailable() {
		return available;
	}

	@Override
	public String toString() {
		return "Offer [lender=" + lender + ", rate=" + rate + ", available=" + available + "]";
	}
}
