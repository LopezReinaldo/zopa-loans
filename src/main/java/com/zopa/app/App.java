package com.zopa.app;

import java.math.BigDecimal;
import java.util.List;

import com.zopa.domain.Offer;
import com.zopa.services.CSVReaderServiceImpl;
import com.zopa.services.CalculatorService;
import com.zopa.services.CalculatorServiceImpl;
import com.zopa.services.ReaderService;

public class App {		
	
	public static void main(String[] args){
		
		if(args.length < 2){
			System.err.println("Invalid number of arguments");
			System.exit(-1);
		}
			
		try{
			String marketData = args[0];
			BigDecimal request = new BigDecimal(args[1]);			
			
			ReaderService readerService = new CSVReaderServiceImpl(marketData);
			
			List<Offer> offers = readerService.getAvailableOffers();
			
			CalculatorService calculatorService = new CalculatorServiceImpl(offers,request);
			
			if(calculatorService.isValidLoanRequest()){
				
				if(calculatorService.amountRequestedIsAvailable()){
					
					System.out.println("Request Amount: £" + String.format("%.0f", request));
					System.out.println("Rate: " + String.format("%.1f", calculatorService.getEffectiveRate().multiply(new BigDecimal(100))) + "%");
					System.out.println("Monthly repayment £" + String.format("%.2f", calculatorService.getMonthlyPayment()));
		            System.out.println("Total repayment: £" + String.format("%.2f", calculatorService.getTotalPayment()));
					
				}else{
					System.out.println("Sorry, it is not possible to provide a quote at this time.");
				}
			}else{
				System.out.println(" The loan must be of any £100 increment between £1000 and £15000 inclusive");
			}
		
		}catch(NumberFormatException e){
			System.err.println("Invalid loan request");
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
		
	}
}
