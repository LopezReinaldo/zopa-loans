package com.zopa.services;

import java.util.List;

import com.zopa.domain.Offer;

public interface ReaderService {

	List<Offer> getAvailableOffers() throws Exception;
}
