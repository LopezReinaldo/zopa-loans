package com.zopa.services;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zopa.domain.Offer;

public class CalculatorServiceImpl implements CalculatorService{
	
	List<Offer> offers;
	BigDecimal requestedLoan;
	
	public CalculatorServiceImpl(List<Offer> offers, BigDecimal requestedLoan) {
		this.offers = offers;
		this.requestedLoan = requestedLoan;
	}

	@Override
	public boolean isValidLoanRequest(){
		if(requestedLoan.compareTo(MIN_VALID_LOAN)>=0 && requestedLoan.compareTo(MAX_VALID_LOAN)<=0){
			if(requestedLoan.remainder(INCREMENT).compareTo(BigDecimal.ZERO) == 0 ) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public BigDecimal getEffectiveRate(){
		
		Map<String, Offer> rates = new HashMap<>();
		
		BigDecimal request = requestedLoan.abs();
		
		for (Offer offer : offers) {
			if(offer.getAvailable().compareTo(request)<0){
				rates.put(offer.getLender(), offer);
				request = request.subtract(offer.getAvailable());
			}else{
				rates.put(offer.getLender(), new Offer(offer.getLender(), offer.getRate(), request));
				break;
			}
		}
		BigDecimal effectiveRate = rates.entrySet().stream().map(e->e.getValue().getRate().multiply(e.getValue().getAvailable())).reduce(BigDecimal.ZERO, BigDecimal::add).divide(requestedLoan);
		return effectiveRate;
	}
	
	@Override
	public boolean amountRequestedIsAvailable(){
		BigDecimal totalAmountAvailable = offers.stream().map(Offer::getAvailable).reduce(BigDecimal.ZERO, BigDecimal::add);
		if(totalAmountAvailable.compareTo(requestedLoan)>=0){
			return true;
		}
		return false;
	}

	private BigDecimal getNominalMonthlyRate(){
		BigDecimal rate = BigDecimal.valueOf(Math.pow(getEffectiveRate().add(BigDecimal.ONE).doubleValue(), BigDecimal.ONE.doubleValue()/new BigDecimal(MONTHS_BY_YEAR).doubleValue())).subtract(BigDecimal.ONE);
		return rate;
	}
	
	@Override
	public BigDecimal getMonthlyPayment() {
		BigDecimal nominalRate = getNominalMonthlyRate();
		BigDecimal numerator = requestedLoan.multiply(nominalRate);
		BigDecimal denominator = BigDecimal.ONE.subtract(nominalRate.add(BigDecimal.ONE).pow(-1*NUMBER_OF_PERIODS, MathContext.DECIMAL128));
		BigDecimal payment = numerator.divide(denominator,RoundingMode.HALF_UP);
		return payment;
	}

	@Override
	public BigDecimal getTotalPayment() {
		BigDecimal total = new BigDecimal(NUMBER_OF_PERIODS).multiply(getMonthlyPayment()); 
		return total;
	}

}
