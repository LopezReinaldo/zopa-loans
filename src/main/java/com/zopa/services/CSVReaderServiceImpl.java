package com.zopa.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.zopa.domain.Offer;

import static java.util.Comparator.comparing;

public class CSVReaderServiceImpl implements ReaderService{
	
	private String filePath;
	private List<Offer> offers = new ArrayList<>();
	public CSVReaderServiceImpl(String filePath) {
		this.filePath = filePath;
	}

	private void processFile() throws IOException{
		try (Stream<String> stream = Files.lines(Paths.get(filePath))) {

			stream.skip(1).forEach(line -> processInput(line));			
			
		} catch (FileNotFoundException | NoSuchFileException e) {
			System.err.println("File not found");
			throw e;
		} catch (IOException e) {
			System.err.println("An error has ocurred processing the file");
			throw e;
		}		
	}
	
	private List<Offer> processInput(String line){
		try{
			String[] input = line.split(",");
			offers.add(new Offer(input[0], new BigDecimal(input[1]), new BigDecimal(input[2])));
			
		}catch(NumberFormatException e){
			System.err.println("Invalid input from file");
		}
		return offers;
	}

	@Override
	public List<Offer> getAvailableOffers() throws IOException{
		processFile();
		offers.sort(comparing(Offer::getRate));
		return offers;
	}
}
