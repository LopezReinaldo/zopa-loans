package com.zopa.services;

import java.math.BigDecimal;

public interface CalculatorService {
	
	BigDecimal MIN_VALID_LOAN = BigDecimal.valueOf(1000);
	BigDecimal MAX_VALID_LOAN = BigDecimal.valueOf(15000);
	BigDecimal INCREMENT = BigDecimal.valueOf(100);	
	int NUMBER_OF_PERIODS = 36;
	int MONTHS_BY_YEAR = 12;
	
	boolean isValidLoanRequest();
	
	boolean amountRequestedIsAvailable();
	
	BigDecimal getEffectiveRate();
	
	BigDecimal getMonthlyPayment();
	
	BigDecimal getTotalPayment();

}
