package com.zopa.services;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.zopa.domain.Offer;

public class CalculatorServiceTest {

	
	private CalculatorService calculatorService;
	
	private List<Offer> offers;
	
	private BigDecimal requestedLoan;
	
	@Before
	public void setup() {
		offers = new ArrayList<>();
		offers.add(new Offer("Mary", new BigDecimal("0.069"), new BigDecimal("480")));
		offers.add(new Offer("Reinaldo", new BigDecimal("0.071"), new BigDecimal("520")));
		requestedLoan = new BigDecimal("1000");
		calculatorService = new CalculatorServiceImpl(offers, requestedLoan);
	}
	
	@Test
	public void testCalculateRate() {
		assertTrue(calculatorService.getEffectiveRate().compareTo(new BigDecimal("0.07004"))==0);
	}
	
	@Test
	public void testMonthlyPayment(){
		assertTrue(calculatorService.getMonthlyPayment().compareTo(new BigDecimal("30.7805943855424254"))==0);
	}
	
	@Test
	public void testTotalPayment(){
		assertTrue(calculatorService.getTotalPayment().compareTo(new BigDecimal("1108.1013978795273144"))==0);
	}

	@Test
	public void testValidRequest(){
		assertTrue(calculatorService.isValidLoanRequest());
	}
	
	@Test
	public void testInvalidRequestByIncrement(){
		requestedLoan = new BigDecimal(1550);
		calculatorService = new CalculatorServiceImpl(offers, requestedLoan);
		assertFalse(calculatorService.isValidLoanRequest());
	}
	
	@Test
	public void testInvalidRequestByLimits(){
		requestedLoan = new BigDecimal(10);
		calculatorService = new CalculatorServiceImpl(offers, requestedLoan);
		assertFalse(calculatorService.isValidLoanRequest());
		
		requestedLoan = new BigDecimal(20000);
		calculatorService = new CalculatorServiceImpl(offers, requestedLoan);
		assertFalse(calculatorService.isValidLoanRequest());
	}
	
	@Test
	public void testAmountAvailable(){
		assertTrue(calculatorService.amountRequestedIsAvailable());
	}
	
	@Test
	public void testNotAmountAvailable(){
		requestedLoan = new BigDecimal(15000);
		calculatorService = new CalculatorServiceImpl(offers, requestedLoan);
		assertFalse(calculatorService.amountRequestedIsAvailable());
	}
}
