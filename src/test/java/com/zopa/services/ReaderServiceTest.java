package com.zopa.services;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.zopa.domain.Offer;


public class ReaderServiceTest {

	ReaderService readerService;
	
	@Before
	public void setup() {
		readerService = new CSVReaderServiceImpl(this.getClass().getClassLoader().getResource("MarketData.csv").getPath());
	}
	
	@Test
	public void testAvailableOffers() throws Exception {
		assertEquals(readerService.getAvailableOffers().size(), 7);
	}

	@Test
	public void testSortedOffers() throws Exception {
		List<Offer> offers = readerService.getAvailableOffers();
		List<Offer> offersSorted = new ArrayList<>(offers);
		Collections.sort(offersSorted, Comparator.comparing(Offer::getRate));
		assertEquals(offersSorted,offers);
	}
}
